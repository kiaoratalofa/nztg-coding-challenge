# NZTG Coding Challenge

My attempt at creating automated tests using JavaScript, Puppeteer, Mocha, and Chai.

## Running the automated tests

Open up a terminal and navigate to the project root directory. Enter the command:

```
npm test
```

## Manual test cases

Note that while some of the manual test cases require opening more than 3 YouTube pages, all automated tests open at most 3 YouTube pages.

### Test 1
```
Test 1: Video length on thumbnail matches video length on video player page

Steps:
1. Open the YouTube home page
2. Enter a search query in the search bar
3. Open the first video from the search results

Expected behaviour: Video length on thumbnail is identical to video length on video player page.
```

### Test 2
```
Test 2: Subscribe count on video player page corresponds to subscribe count on uploader's channel

Steps:
1. Open the YouTube home page
2. Enter a search query in the search bar
3. Open the first video from the search results
4. Open the video uploader's channel

Let vidPageSubs be the subscribe count displayed on the video player page and channelSubs be the subscribe count displayed on the uploader's channel. 

Expected behaviour: 
	if channelSubs <= 999
		vidPageSubs == channelSubs
	else if channelSubs <= 9,999
		vidPageSubs == channelSubs rounded down from the nearest hundred
	else if channelSubs <= 999,999 
		vidPageSubs == channelSubs rounded down from the nearest thousand
	else if channelSubs <= 9,999,999
		vidPageSubs == channelSubs rounded down from the nearest hundred thousand
	else if channelSubs <= 999,999,999 
		vidPageSubs == channelSubs rounded down from the nearest million
```

### Test 3
```
Test 3: New videos have the "New" badge attached to them in the search results

Steps:
1. Open the YouTube home page
2. Enter a search query in the search bar
3. Filter the results so that only videos uploaded today are listed

Expected behaviour: The first video should have the "New" badge in the search results.
```

### Test 4
```
Test 4: Videos published today have today's date on the video player page

Steps:
1. Open the YouTube home page
2. Enter a search query in the search bar
3. Filter the results so that only videos uploaded today are listed
4. Open the first video from the search results

Expected behaviour: The published date of the video is the same as today's date.
```

### Test 5
```
Test 5: Trending label corresponds to video's position on the trending list.

Steps:
1. Open the YouTube trending page
2. Open the #27 trending video

Expected behaviour: The video player page of the #27 trending video has the label "#27 ON TRENDING" above the title.
```

## Built With

* [JavaScript](https://www.javascript.com/) - Programming language
* [npm](https://www.npmjs.com/) - Dependency management
* [Puppeteer](https://developers.google.com/web/tools/puppeteer/) - WebDriver library
* [Mocha](https://mochajs.org/) - Testing framework
* [Chai](https://www.chaijs.com/) - Assertion library


## Authors

* **Alexander Michael**
