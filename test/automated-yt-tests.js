var assert = require('chai').assert;
var moment = require('moment');
const puppeteer = require('puppeteer');
const testHelper = require('./test-helper');
const YOUTUBE_URL = (query) => `https://www.youtube.com/${query}`;

describe ('Non-Headless Tests', function() {
	describe('Test 1', function() {
		it('Video length on thumbnail matches video length on video player page', async() => {
			const browser = await puppeteer.launch({
				headless: false, // need to disable headless because we are going to use an extension
				args: ['--disable-extensions-except=./ublock'] // use uBlock Origin to prevent waiting for ads
			});
			const page = await browser.newPage();
			var selector;

			// search for a video
			await page.goto(YOUTUBE_URL('results?search_query=rick+roll'), {waitUntil: 'networkidle2'});

			// look at first video's thumbnail and grab video length displayed on it
			selector = '#page-manager > ytd-search #overlays > ytd-thumbnail-overlay-time-status-renderer > span';
			await page.waitForSelector(selector, {visible: true});
			const thumbnailVidLength = await page.evaluate((selector) => {
				return document.querySelector(selector).innerText;
			}, selector);

			// open up the video
			await page.click('#contents #video-title');

			// grab video length on the video player page
			selector = '.ytp-time-duration';
			await page.waitForSelector(selector, {visible: true});
			const playerVidLength = await page.evaluate((selector) => {
				return document.querySelector(selector).innerText;
			}, selector);

			await browser.close();
			assert.equal(playerVidLength, thumbnailVidLength);
		});
	});
});

describe ('Headless Tests', function() {
	before(async () => {
		browser = await puppeteer.launch({
			headless: true
		});
	});

	after(async () => {
		await browser.close();
	});

	describe('Test 2', function() {
		it('Subscribe count on video player page corresponds to subscribe count on uploader\'s channel', async() => {
			const context = await browser.createIncognitoBrowserContext();
			const page = await context.newPage();
			var selector;
			
			// search for a video
			await page.goto(YOUTUBE_URL('results?search_query=jre'), {waitUntil: 'networkidle2'});

			// open up the first video
			await page.click('#contents ytd-video-renderer #video-title');

			// grab the subscribe count on the video player page
			selector = '#meta-contents #subscribe-button';
			await page.waitForSelector(selector, {visible: true});
			var vidPageSubs = await page.evaluate((selector) => {
				return document.querySelector(selector).innerText;
			}, selector);
			vidPageSubs = testHelper.vidPageSubsToNum(vidPageSubs);

			// open up the uploader's channel
			await page.click('#upload-info #channel-name');

			// grab the subscribe count on the uploader's channel
			selector = '#subscriber-count';
			await page.waitForSelector(selector, {visible: true});
			var channelSubs = await page.evaluate((selector) => {
				return document.querySelector(selector).innerText;
			}, selector);
			channelSubs = testHelper.channelSubsToNum(channelSubs);

			await context.close();
			assert.equal(testHelper.roundChannelSubs(channelSubs), vidPageSubs);
		});
	});

	describe('Test 3', function() {
		it('New videos have the "New" badge attached to them in the search results', async() => {
			const context = await browser.createIncognitoBrowserContext();
			const page = await context.newPage();
			var selector;

			// search for a video
			await page.goto(YOUTUBE_URL('results?search_query=food'), {waitUntil: 'networkidle2'});

			// click on search filter
			await page.click('#container > ytd-toggle-button-renderer');

			// filter for videos uploaded today
			await page.click('div[title="Search for Today"]');
			
			// grab all badges on the first video
			selector = '#contents > ytd-video-renderer:nth-child(1) #badges > div';
			await page.waitForSelector(selector, {visible: true});
			var badges = await page.evaluate((selector) => {
				return Array.from(document.querySelectorAll(selector)).map(x => x.innerText);
			}, selector);
			
			await context.close();
			assert.equal(badges.includes('New'), true);
		});
	});

	describe('Test 4', function() {
		it('Videos published today have today\'s date on the video player page', async() => {
			const context = await browser.createIncognitoBrowserContext();
			const page = await context.newPage();
			var selector;
			
			// search for a video
			await page.goto(YOUTUBE_URL('results?search_query=food'), {waitUntil: 'networkidle2'});

			// click on search filter
			await page.click('#container > ytd-toggle-button-renderer');

			// filter for videos uploaded today
			await page.click('div[title="Search for Today"]');

			// wait for filter to be applied
			await page.waitForSelector('div[title="Search for Today"]', {hidden: true});

			// click on video
			selector = '#contents #video-title';
			await page.waitForSelector(selector, {visible: true});
			await page.click(selector);

			// grab published date of video
			selector = '#upload-info > span';
			await page.waitForSelector(selector, {visible: true});
			var publishedDate = await page.evaluate((selector) => {
				return document.querySelector(selector).innerText.replace('Published on ', '').replace(',', '');
			}, selector);
			
			// get today's date on this host
			var today = moment().format('MMM') + ' ' + moment().format('D') + ' ' + moment().format('YYYY');
			
			await context.close();
			assert.equal(publishedDate, today);
		});
	});

	describe('Test 5', function() {
		// trendingPos should be between 1-50 (max of 50 vids are on trending)
		const trendingPos = 27;

		it('The #' + trendingPos + ' trending video has the label "#' + trendingPos + ' ON TRENDING" above its title', async() => {
			const context = await browser.createIncognitoBrowserContext();
			const page = await context.newPage();
			var selector;
			
			// go to trending page
			await page.goto(YOUTUBE_URL('feed/trending'), {waitUntil: 'networkidle2'});

			// click on a video on trending
			await page.click(`#grid-container > ytd-video-renderer:nth-child(${trendingPos})`);

			// grab trending label on video player page
			selector = '#container > yt-formatted-string > a';
			await page.waitForSelector(selector, {visible: true});
			var trendingLabel = await page.evaluate((selector) => {
				return document.querySelector(selector).innerText;
			}, selector);

			await context.close();
			assert.equal(trendingLabel,`#${trendingPos} ON TRENDING`);
		});
	});
});
