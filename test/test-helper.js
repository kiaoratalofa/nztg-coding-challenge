module.exports = {
	roundChannelSubs: function(channelSubs) {
		if (channelSubs <= 999) { // under 1K
			return channelSubs; // don't bother rounding
		} else if (channelSubs <= 9999) { // under 10k
			return Math.floor(channelSubs / 100) * 100; // round down from nearest hundred
		} else if (channelSubs <= 999999) { // under 999k
			return Math.floor(channelSubs / 1000) * 1000; // round down from nearest thousand
		} else if (channelSubs <= 9999999) { // under 9M
			return Math.floor(channelSubs / 100000) * 100000; // round down from nearest hundred thousand
		}  else if (channelSubs <= 999999999) { // under 999M
			return Math.floor(channelSubs / 1000000) * 1000000; // round down from nearest million
		} else {
			// throw an exception	
		}
	},
	vidPageSubsToNum: function(vidPageSubs) {
		var result = vidPageSubs.replace('SUBSCRIBE ', ''); // remove surrounding text from sub count

		// convert the labeled sub count to a proper number
		if (vidPageSubs.includes('M')) {
			result = result.replace('M', '');
			result = result * Math.pow(10, 6);
		} else if (result.includes('K')) {
			result = result.replace('K', '');
			result = result * Math.pow(10, 3);
		}

		return result;
	},
	channelSubsToNum: function(channelSubs) {
		return channelSubs.replace(' subscribers', '').replace(/,/g, ''); // remove surrounding text and commas from sub count
	}
}
